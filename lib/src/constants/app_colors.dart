import 'dart:ui';

class AppColors {
  static const rainBlueLight = Color(0xFF4480C6);
  static const rainBlueDark = Color(0xFFFF7F7F);
  static const rainGradient = [rainBlueLight, rainBlueDark];
  static const accentColor = Color(0xFFFF0000);
}
